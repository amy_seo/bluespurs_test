package com.bluespurs.starterkit.service;

import org.json.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Find cheapest product at Walmart
 * Created by Amy on 01/06/2016.
 */
public class WalmartSearch {
    String url = "";
    String apiKey = "";

    String currency = "CND";
    String store = "Walmart";

    public WalmartSearch(String apiKey){
        this.apiKey = apiKey;
    }

    public void setName(String name){
        String link = "http://api.walmartlabs.com/v1/search?apiKey=";
        link+= apiKey;
        link += "&query="+name+"&categoryId=3944&sort=price&ord=asc&numItems=1&show=name,price";
        url = link;
    }

    /**
     * Request the lowest priced product with the given 'name'
     *
     * @param name Name of the product
     * @return Product with the lowest price
     * @throws IOException IO Exception (e.g. Malformed URL)
     */
    public Product find(String name)throws IOException {
        setName(name);
        URL obj = new URL(url);
        HttpURLConnection request = (HttpURLConnection) obj.openConnection();
        request.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(
                new InputStreamReader(request.getInputStream()));
        String inputLine;
        String response = "";

        while ((inputLine = in.readLine()) != null) {
            response+=inputLine;
        }
        in.close();

        JSONObject json = new JSONObject (response);
        JSONArray jsonItem = json.getJSONArray("items"); // Because Walmart search API limited to 1, only 1
        // least expensive item will be returned


        String productName = (String) jsonItem.getJSONObject(0).getString("name");
        double salePrice = jsonItem.getJSONObject(0).getDouble("salePrice");

        Product product = new Product(productName, salePrice, currency, store);

        return product;
    }
}
