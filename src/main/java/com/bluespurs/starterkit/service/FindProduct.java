package com.bluespurs.starterkit.service;

import java.io.IOException;

/**
 * Find the cheapest product from BestBuy or Walmart
 *
 * I am unfamiliar with GET and RESTful API's. I have tried my best
 * to research during this time, and have attempted my best to build
 * a skeleton program for the given task.
 *
 * Notes:
 * 1) The program assumes currency is equal for BesyBuy and Walmart.
 *
 * Created by Amy on 01/06/2016.
 */
public class FindProduct {
    /**
     * Find the lowest priced item from BestBuy or Walmart
     * @param name Name of the product
     * @throws IOException Exception
     */
    public Product find(String name) throws IOException{
        BestBuySearch bbs = new BestBuySearch("pfe9fpy68yg28hvvma49sc89");
        WalmartSearch ws = new WalmartSearch("rm25tyum3p9jm9x9x7zxshfa");

        Product bestBuy = bbs.find(name);
        Product walmart = ws.find(name);

        if(bestBuy.getPrice() > walmart.getPrice()){
            // Best Buy product cheapest
            return bestBuy;
        }
        else if(bestBuy.getPrice() < walmart.getPrice()){
            // Walmart product cheapest
            return walmart;
        }
        else{
            // Price for BestBuy and Walmart equal. Break ties or print both Walmart and BestBuy
            bestBuy.setStore("BestBuy and Walmart");
            return bestBuy;
        }
    }

    /**
     * Test main method
     * @param args
     */
    public static void main(String[] args){
        FindProduct fp = new FindProduct();
        Product bestProduct = null;
        try {
            bestProduct = fp.find("android");
        }catch(IOException e){
            e.printStackTrace();
        }

        if(bestProduct == null){ // If bestProduct is null, this means there was no result returned
            System.out.println("No results.");
        }
        else {
            System.out.println(bestProduct.toJSON());
        }
    }
}