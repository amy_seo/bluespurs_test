package com.bluespurs.starterkit.service;
import org.json.*;
/**
 * Represents a product returned from BestBuy or Walmart.
 *
 * 1) This class was created to be one single object for BestBuy and Walmart.
 *      Two API's have different JSON structures, so a common Object is used.
 * 2) Furthermore, I could not find a way to dynamically add a new "location":"Bestbuy/Walmart"
 *      attribute to the returned object, and have resorted to using a new Object.
 *
 * Created by Amy on 01/06/2016.
 */
public class Product {
    String name;
    double price;
    String currency;
    String store;

    Product(String name, double price, String currency, String store){
        String quoted = name.replaceAll("\"", "\\\\\"");
        this.name = quoted;
        this.price = price;
        this.currency = currency;
        this.store = store;
    }

    public double getPrice(){
        return price;
    }

    public String getName(){
        return name;
    }

    public String getCurrency() {
        return currency;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store){
        this.store = store;
    }

    /**
     * Converts to String form
     * @return
     */
    public String toString(){
        String str = "{\n\t\"productName\":\""+name+"\",";
        str+= "\n\t\"bestPrice\":\""+price+"\"," ;
        str+= "\n\t\"currency\":\""+currency+"\",";
        str+= "\n\t\"location\":\""+store+"\"";
        str+= "\n}";
        return str;
    }

    public JSONObject toJSON(){
        JSONObject json = new JSONObject(toString());
        return json;
    }
}
