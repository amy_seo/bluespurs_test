package com.bluespurs.starterkit.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Find product at BestBuy
 * Created by Amy on 01/06/2016.
 */
public class BestBuySearch {

    String url = "";
    String apiKey = "";
    String currency = "CND";
    String store = "BestBuy";

    public BestBuySearch(String apiKey){
        this.apiKey = apiKey;
        url+=apiKey;
    }

    public void setName(String name){
        String link = "http://api.bestbuy.com/v1/products(search=";
        link += name;
        link += ")?show=name,salePrice&sort=salePrice.asc&format=json&pageSize=1&page=1&apiKey="; // Sort by salePrice (ascending)
        // PageSize & Page limits to 1 unique result (if there is a returned value)
        link += apiKey;
        url = link;
    }

    /**
     * Find should use the BestBuy API to retreive the product that has the lowest selling price.
     * @param name Name of the product
     * @return Product object
     * @throws IOException IO Exception (e.g. Malfunctioned URL)
     */
    public Product find(String name)throws IOException{
        setName(name);
        URL obj = new URL(url);
        HttpURLConnection request = (HttpURLConnection) obj.openConnection();
        request.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(
                new InputStreamReader(request.getInputStream()));
        String inputLine;
        String response = "";

        while ((inputLine = in.readLine()) != null) {
            response+=inputLine;
        }
        in.close();

        JSONObject json = new JSONObject (response);
        int numItems = json.getInt("total"); // Find number of items returned

        if(numItems <= 0)
            return null;

        JSONArray jsonItem = json.getJSONArray("products"); // Because Walmart search API limited to 1, only 1
        // least expensive item will be returned

        String productName = (String) jsonItem.getJSONObject(0).getString("name");
        double salePrice = jsonItem.getJSONObject(0).getDouble("salePrice");

        Product product = new Product(productName, salePrice, currency, store);

        return product;
    }
}
